//Zmienne globalne
let $list; let $input; let $addBtn; let $btnGroup;


const initialList = ['Clean room', 'Feed yor pet friend', 'Prepare office'];

function main() {
    prepareDOMElements();
    prepareInitialList();
    prepareDOMEvents();
}

function prepareDOMElements() {
    //przygotowanie - wyszukanie elementow w drzewie DOM
    $list = document.getElementById('list');
    $input = document.getElementById('myInput');
    $addBtn = document.getElementById('addToDo');
    $btnGroup = document.createElement("button").innerHTML = "Mark as done "

    $input.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            initialList.push($input.value);
            prepareInitialList();
        }
    });
}

function prepareInitialList() {
    $list.innerHTML = "";
    //wrzucenie poczatkowych elementow do listy
    initialList.forEach(function (todo, i) {
        addNewElementToList(todo, i);
    });
}

function addNewElementToList(title, index) {
    //obsługa dodawanie elementów do listy
    const newElement = createElement(title, index);
    $list.appendChild(newElement);
    
}

function createElement(title, index) {
    // Tworzyc reprezentacje DOM elementu return newElement (tworzy nowa linijke listy)
    // return newElement
    const newElement = document.createElement('li');
    newElement.innerHTML = title + '<div class="custom-buttons"><button type="button" onclick="removeListElement(' + index + ')" class="btn btn-secondary">Delete</button>' +
        '<button type="button" onclick="editListElement(' + index + ')" class="btn btn-secondary">Edit</button>' +
        '<button type="button" onclick="markElementAsDone(' + index + ')"class="btn btn-secondary">Mark as done</button></div>';

    if (title.endsWith(" -> done")) {
        newElement.style.textDecoration = 'line-through';
    }

    return newElement;
}

function prepareDOMEvents() {
    // przygotowanie listenerow
    $addBtn.addEventListener("click", addButtonClickHandler);
}

function addButtonClickHandler() {
    //obługa kliknięcia przycisku dodaj
    initialList.push($input.value)
    prepareInitialList();
}
function removeListElement(index) {
    // Usuwanie elementu z listy
    initialList.splice(index, 1);
    prepareInitialList();
}
function editListElement(index) {
    var changedToDoItem = prompt("Update todo item:", initialList[index]);
    initialList[index] = changedToDoItem;
    prepareInitialList();
}

function markElementAsDone(index) {
    if (initialList[index].endsWith(" -> done")) {
        // do nothing ... item has been already marked as done.
    }
    else {
        initialList[index] = initialList[index] + " -> done"
    }
    prepareInitialList();
}

document.addEventListener('DOMContentLoaded', main);